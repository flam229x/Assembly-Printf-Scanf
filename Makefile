build=main.o printf.o scanf.o

all: build

build: $(build)
	ld $(build) -o main.exe

%.o: %.asm
	nasm -felf64 $< -o $@

clean:
	@rm -vf *.o *.exe
