%define next_arg rbp + rbx*8 + 16

%macro popaq 0
    pop rdi
    pop rsi
    pop rbp
    pop rbx
    pop rdx
    pop rcx
    pop rax
%endmacro


%macro pushaq 0
    push rax
    push rcx
    push rdx
    push rbx
    push rbp
    push rsi
    push rdi
%endmacro


%macro CHECK_NUMBER 1
    %assign a '0'
    
    %rep 10
        cmp byte %1, a
        je %%find

        %assign a a+1
    %endrep

    %%find:

%endmacro


%macro READ_CHAR 2
    pushaq

    mov rax, 0x00
    mov rdi, %2
    lea rbx, [%1]
    mov rsi, rbx
    mov rdx, 1

    syscall
    
    popaq
%endmacro


%macro CHECK_SPECIAL 1
        push rax
        push rcx
        push rdi
        push qword 0x0a2009

        mov rdi, rsp
        
        xor rax, rax
        mov al, %1
        mov rcx, 3
        repne scasb

        pop rdi
        pop rdi
        pop rcx
        pop rax 
%endmacro

%macro PRINT_CHAR 2
    pushaq
    push qword %1

    mov rax, 0x01
    mov rdi, %2
    mov rsi, rsp
    mov rdx, 1

    syscall

    add rsp, 8
    popaq
%endmacro


%macro Printf 0
    mov rdi, 0x01
    call Fprintf
%endmacro


%macro Scanf 0
    mov rdi, 0x00
    call Fscanf
%endmacro


%macro EXIT_SUCC 0
    mov rax, 0x3c
    mov rdi, 0

    syscall
%endmacro


