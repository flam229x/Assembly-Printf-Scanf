%define next_arg rbp + rbx*8 + 16

%include "macroses.inc"

extern Fscanf
extern Fprintf
extern ReadString

section .data
    string  times 256 db 0
    decimal dq 0
    symbol  db 0

    format  db ", %d", 0x00
    format2 db "hex:0x%x", 0x0a, 0x00
    input   db "input.txt", 0x00
    output  db "output.txt", 0x00
    fd      db 0x0

section .text
    global _start

_start:
    ; mov rax, 0x02
    ; mov rdi, input
    ; mov rsi, 2
    ; mov rdx, 0666o
    ; syscall

    mov rdi, 0x0
    push decimal
    mov rax, format
    call Fscanf
    add rsp, 24
    
    ; mov rax, 0x02
    ; mov rdi, output
    ; mov rsi, 0x241
    ; mov rdx, 0666o
    ; syscall 

    mov rdi, 0x01
    push qword [decimal]
    mov rax, format2
    call Fprintf
    add rsp, 24

    EXIT_SUCC
